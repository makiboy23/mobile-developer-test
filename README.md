# Mobile Developer Test

Mobile Developer Test - iOS App

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Goals

- [x] Create an app for Smartphones and Tablets that queries a web service API for catalog content, parses the JSON-formatted response and displays the data. The data consists of a list of products that should be shown in a vertical scrolling list. (See 'Requirements' section for catalog information)  
- [x] The UI is adaptive to portrait and landscape orientations and maintains it’s state across orientation changes  
- [x] In the product list show the product's image, name, price and brand  
- [x] Implement Pull-To-Refresh  
- [x] Implement automatic loading more when the user scrolls to the bottom of the list (aka  infinite scrolling)  
- [x] Add UI for sort functionality on all sort orders (see specification table below).  
- [x] Add offline functionality (Cache images, save data within a local persistence store, etc.)  
- [] Select at least one class in your code base that you provide a unit tests for  

- [x] Separate the view and the controller logic in an MVP style
- [] Cover logic with unit tests
- [x] Add a detail view that shows additional data about the vehicle

- [x] You may use third party libraries to implement your solution faster.
- [x] Code must be under version control, please provide a README with the code
- [x] Code can be submitted by either providing a link to the repository or providing the repo
in a packaged way
- [x] When submitting your code, please add information about the IDE you've utilized during
the test.
- [] Operating system requirements Apple iOS: > 8.0 Android: > 4.1 (API level 16) - tested in 9.0 only :(
* issue constraints in 8.0 :( - if there's an extra time i will fix it.

- [x] We value quality and consistency over feature-completeness. Therefore please make
sure that you consider the official guidelines to provide a good read of your code via
consistent code style and in-line documentation.

- [x] You can query the catalog here:
https://www.carmudi.co.id/api/cars/page:{page}/maxitems:{num_items_per_page}/

## Pagination, Reload and Sorting logic
```
let itemMultiplier = indexPage * limit // current page index multiply to limit
```
Get current index/number of page and multiply to number of items/limit, to request maxItem in the server. 


## Prerequisites

XCode IDE Latest

## CocoaPods 
```
Alamofire ‘~> 4.5’
```

## Enumeration
* SortType : Hold kind of sort types

## Extensions
* Alamofire : Extension compose of function that decode the json data and return products Object. Also saving data in UserDefaults

* Formatter : Extension that Return formatted currency number

* UIView : Extension for SafeAnchor : NSLayoutConstraint

* UserDefaults :   Extension for set and get for saving locally

 


