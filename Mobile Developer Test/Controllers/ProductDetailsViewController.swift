//
//  ProductDetailsViewController.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 21/06/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import UIKit

class ProductDetailsViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupNavigationBar()
    }
    
    var product: Product? {
        didSet {
            titleLabel.text = product?.productName ?? ""
        }
    }
    
    let cellId = "cellId"
    let cellCoverId = "cellCoverId"
    
    // MARK:- SETUPVIEWS
    
    fileprivate func setupViews() {
        collectionView?.backgroundColor = .white
        collectionView?.register(ProductDetailsCoverCell.self, forCellWithReuseIdentifier: cellCoverId)
        collectionView?.register(ProductDetailsCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 9
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.item == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellCoverId, for: indexPath) as! ProductDetailsCoverCell
            cell.product = product
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ProductDetailsCell
            if indexPath.item == 1 {
                cell.productLabel.text = "Name"
                cell.valueLabel.text = product?.productName ?? ""
            } else if indexPath.item == 2 {
                cell.productLabel.text = "Brand"
                cell.valueLabel.text = product?.productBrand ?? ""
            } else if indexPath.item == 3 {
                cell.productLabel.text = "Price"
                let price = product?.productPrice ?? "0"
                cell.valueLabel.text = "Rp \(price.formattedWithSeparator)" // Indonesian rupiah currency
            } else if indexPath.item == 4 {
                cell.productLabel.text = "Condition"
                cell.valueLabel.text =  product?.productCondition ?? ""
            } else if indexPath.item == 5 {
                cell.productLabel.text = "Contact Person"
                cell.valueLabel.text = product?.personContactName ?? ""
            } else if indexPath.item == 6 {
                cell.productLabel.text = "Email"
                cell.valueLabel.text = product?.personContactEmail ?? ""
            } else if indexPath.item == 7 {
                cell.productLabel.text = "Mobile"
                cell.valueLabel.text = product?.personContactMobile ?? ""
            } else if indexPath.item == 8 {
                cell.productLabel.text = "Phone Number"
                cell.valueLabel.text = product?.personContactHomePhone ?? ""
            }
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item == 0 {
            return CGSize(width: view.frame.width, height: view.frame.width)
        } else {
            return CGSize(width: view.frame.width, height: 70)
        }
    }
    
    // MARK:- SETUP NAVIGATION BAR
    
    let titleLabel = UILabel()
    
    func setupNavigationBar() {
        navigationItem.title = ""
        navigationController?.navigationBar.isTranslucent = false

        titleLabel.textColor = .darkGray
        titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        titleLabel.textAlignment = .center
        navigationItem.titleView = titleLabel
    }
}
















