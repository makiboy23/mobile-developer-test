//
//  ProductListingViewController.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 20/06/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import UIKit

class ProductListingViewController: BaseViewController<ProductListingCell, Product>, UICollectionViewDelegateFlowLayout {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionViews()
        setupNavigationBar()
        fetchProducts()
    }
    
    override var items: [Product]! {
        didSet {
            isLoadingFlag = false
            collectionView?.reloadData()
            endRefreshing()
            handleHidePagination()
        }
    }
    
    let headerCellId = "headerCellId"
    
    var cellRatio = (UIScreen.main.bounds.width / 2) - 20
    var isLoadingFlag = false
    var indexPage = 1
    
    var limit = 10
    
    let refreshControl: UIRefreshControl = {
        let rc = UIRefreshControl()
        rc.tintColor = .darkGray
        rc.addTarget(self, action: #selector(handlePullToRefresh), for: .valueChanged)
        return rc
    }()
    
    let paginationLoaderView: PaginationLoaderView = {
        let view = PaginationLoaderView()
        view.layer.cornerRadius = 15
        view.layer.masksToBounds = true
        view.alpha = 0.8
        return view
    }()
    
    func setupCollectionViews() {
        if UIDevice.current.userInterfaceIdiom == .pad {
            cellRatio = (UIScreen.main.bounds.width / 4) - 20
            limit = 20
        } else {
            cellRatio = (UIScreen.main.bounds.width / 2) - 20
            limit = 10
        }
        
        // setup header cell
        collectionView?.register(EmptyListCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerCellId)
        collectionView?.addSubview(refreshControl)
        setupPaginationLoaderView()
    }
    
    // MARK:- ACTIONS and FUNCTIONS
    
    func navigateToProductDetails(product: Product) {
        let layout = UICollectionViewFlowLayout()
        let controller = ProductDetailsViewController(collectionViewLayout: layout)
        controller.product = product
        navigationController?.pushViewController(controller, animated: true)
    }
    
    // MARK: PULL to REFRESH - REFRESH
    
    @objc func handlePullToRefresh() {
        if !isLoadingFlag {
            fetchProducts()
        }
    }
    
    func endRefreshing() {
        // add 0.5 delay
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (timer) in
                self.refreshControl.endRefreshing()
            })
        } else {
            // Fallback on earlier versions
            self.refreshControl.endRefreshing()
        }
    }
    
    // MARK: LOAD MORE - LAZY LOAD - PAGINATION
    
    func handlePagination() {
        handleShowPagination()
        fetchProductsPagination(page: indexPage)
    }
    
    func handleShowPagination() {
        self.paginationViewBottomConstraint.constant = 0
    }
    
    func handleHidePagination() {
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false, block: { (timer) in
                self.paginationViewBottomConstraint.constant = 200
            })
        } else {
            // Fallback on earlier versions
            self.paginationViewBottomConstraint.constant = 200
        }
    }
    
    // MARK:- FETCH DATA
    
    func fetchProducts() {
        isLoadingFlag = true
        let itemMultiplier = indexPage * limit // current page index multiply to limit
        APIServices.shared.fetchProducts(page: indexPage, maxItem: itemMultiplier, sortType: sortType) { (products) in
            // if no data found load offline data
            if products.count == 0 {
                self.items = UserDefaults.standard.savedProducts()
                return
            }
            
            self.items = products
        }
    }
    
    func fetchProductsPagination(page: Int = 1) {
        // do append new products
        guard let products = self.items else { return }
        APIServices.shared.fetchProducts(products: products, page: page, maxItem: limit, sortType: sortType) { (newProducts) in
            self.items = newProducts
        }
    }
    
    func fetchProductsSort(sortType: SortTypes) {
        let itemMultiplier = indexPage * limit // current page index multiply to limit
        APIServices.shared.fetchProducts(page: indexPage, maxItem: itemMultiplier, sortType: sortType) { (products) in
            self.items = products
        }
    }
    
    // MARK: SETUP PAGINATIONVIEW
    
    var paginationViewBottomConstraint: NSLayoutConstraint!
    
    fileprivate func setupPaginationLoaderView() {
            view.addSubview(paginationLoaderView)
            paginationLoaderView.translatesAutoresizingMaskIntoConstraints = false
            paginationViewBottomConstraint = paginationLoaderView.bottomAnchor.constraint(equalTo: view.safeBottomAnchor, constant: 200)
            paginationViewBottomConstraint.isActive = true
            paginationLoaderView.heightAnchor.constraint(equalToConstant: 30).isActive = true
            paginationLoaderView.widthAnchor.constraint(equalToConstant: 30).isActive = true
            paginationLoaderView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let headerCell = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerCellId, for: indexPath) as! EmptyListCell
        
        return headerCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if let _ = items?.count {
            return .zero
        }
        return view.frame.size
    }
    
    // MARK:- SCROLLVIEW
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        
        if offsetY == collectionView?.contentOffset.y && offsetY > 0{
            // do load more
            if !isLoadingFlag {
                indexPage += 1
                handlePagination()
            }
        }
    }
    
    // MARK:- COLLECTIONVIEWS
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let product = items?[indexPath.item] else { return }
        navigateToProductDetails(product: product)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = items?.count {
            return count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // 1:1 ratio for image
        // 80 to details
        // iPad
        return CGSize(width: cellRatio, height: cellRatio + 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 20, left: 15, bottom: 0, right: 15)
    }
    
    // MARK:- SETUP NAVIGATION BAR
    
    let titleLabel = UILabel()
    
    func setupNavigationBar() {
        navigationItem.title = ""
        navigationController?.navigationBar.isTranslucent = false
        
        titleLabel.text = "  Product Listing"
        titleLabel.frame = CGRect(x: 0, y: 0, width: view.frame.width - 50, height: (navigationController?.navigationBar.frame.height)!)
        titleLabel.textColor = .darkGray
        titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        navigationItem.titleView = titleLabel
        
        setupNavigationButtons()
    }
    
    private func setupNavigationButtons() {
        let olderNewestImage = UIImage(named: "sortDate")?.withRenderingMode(.alwaysTemplate)
        let olderNewestSortButtonItem = UIBarButtonItem(image: olderNewestImage, style: .plain, target: self, action: #selector(handleSortOlderNewest))
        olderNewestSortButtonItem.tintColor = .darkGray
        
        let priceImage = UIImage(named: "sortPrice")?.withRenderingMode(.alwaysTemplate)
        let priceSortButtonItem = UIBarButtonItem(image: priceImage, style: .plain, target: self, action: #selector(handleSortPrice))
        priceSortButtonItem.tintColor = .darkGray
        
        let milesImage = UIImage(named: "sortMiles")?.withRenderingMode(.alwaysTemplate)
        let milesSortButtonItem = UIBarButtonItem(image: milesImage, style: .plain, target: self, action: #selector(handleSortMiles))
        milesSortButtonItem.tintColor = .darkGray
        
        // searchButtonItem
        navigationItem.rightBarButtonItems = [olderNewestSortButtonItem, priceSortButtonItem, milesSortButtonItem]
    }
    
    // MARK: NAVIGATIONBAR ACTIONS
    
    var olderNewestSortFlag = false
    var priceSortFlag = false
    var milesSortFlag = false
    var sortType: SortTypes?
    
    @objc func handleSortOlderNewest() {
        olderNewestSortFlag = (olderNewestSortFlag ? false : true)
        sortType = (olderNewestSortFlag ? SortTypes.sortAscDate : SortTypes.sortDescDate)
        fetchProductsSort(sortType: sortType!)
    }
    
    @objc func handleSortPrice() {
        priceSortFlag = (priceSortFlag ? false : true)
        sortType = (priceSortFlag ? SortTypes.sortAscPrice : SortTypes.sortDescPrice)
        fetchProductsSort(sortType: sortType!)
    }
    
    @objc func handleSortMiles() {
        milesSortFlag = (milesSortFlag ? false : true)
        sortType = (milesSortFlag ? SortTypes.sortAscMiles : SortTypes.sortDescMiles)
        fetchProductsSort(sortType: sortType!)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.view.layoutIfNeeded()
    }
}






















