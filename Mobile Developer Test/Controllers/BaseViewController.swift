//
//  BaseViewController.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 06/07/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import UIKit

class BaseViewController<T: BaseCell<U>, U>: UICollectionViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    let cellId = "cellId"
    
    var items: [U]! {
        didSet {
            collectionView?.reloadData()
        }
    }
    
    // MARK:- SETUPVIEWS
    
    fileprivate func setupViews() {        
        // setup collectionview
        collectionView?.backgroundColor = .white
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.register(T.self, forCellWithReuseIdentifier: cellId)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! BaseCell<U>
        cell.item = items[indexPath.row]
        return cell
    }
}







