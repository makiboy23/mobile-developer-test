//
//  ProductDetailsCell.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 21/06/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import UIKit

class ProductDetailsCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .lightGray
        return view
    }()
    
    let productLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let valueLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    // MARK:- SETUPVIEWS
    
    fileprivate func setupViews() {
        addSubview(separatorView)
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        separatorView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        separatorView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        separatorView.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        addSubview(productLabel)
        productLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10).isActive = true
        productLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        productLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
        
        addSubview(valueLabel)
        valueLabel.topAnchor.constraint(equalTo: productLabel.bottomAnchor, constant: 10).isActive = true
        valueLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 10).isActive = true
        valueLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}















