//
//  ProductListingCell.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 20/06/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import UIKit

class ProductListingCell: BaseCell<Product> {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    override var item: Product! {
        didSet {
            productNameLabel.text = item.productName
            productBrandLabel.text = item.productName

            let price = item.productPrice ?? "0"
            productPriceLabel.text = "Rp \(price.formattedWithSeparator)" // Indonesian rupiah currency

            guard let imageUrl = item.productImageUrl else { return }
            productImageView.loadImage(urlString: imageUrl)
        }
    }
    
    let productImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = .darkGray
        iv.layer.cornerRadius = 6
        iv.layer.masksToBounds = true
        iv.tintColor = .lightGray
        iv.clipsToBounds = true
        iv.image = UIImage(named: "car-placeholder")?.withRenderingMode(.alwaysTemplate)
        return iv
    }()
    
    let productNameLabel: UILabel = {
        let lbl = UILabel()
        return lbl
    }()
    
    let productBrandLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .lightGray
        return lbl
    }()
    
    let productPriceLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .purple
        lbl.font = UIFont.systemFont(ofSize: 15)
        return lbl
    }()
    
    // MARK:- SETUPVIEWS
    
    fileprivate func setupViews() {
        addSubview(productImageView)
        productImageView.translatesAutoresizingMaskIntoConstraints = false
        productImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        productImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        productImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        productImageView.heightAnchor.constraint(equalTo: productImageView.widthAnchor).isActive = true
        
        addSubview(productNameLabel)
        productNameLabel.translatesAutoresizingMaskIntoConstraints = false
        productNameLabel.topAnchor.constraint(equalTo: productImageView.bottomAnchor, constant: 5).isActive = true
        productNameLabel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        productNameLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    
        addSubview(productBrandLabel)
        productBrandLabel.translatesAutoresizingMaskIntoConstraints = false
        productBrandLabel.topAnchor.constraint(equalTo: productNameLabel.bottomAnchor).isActive = true
        productBrandLabel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        productBrandLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        addSubview(productPriceLabel)
        productPriceLabel.translatesAutoresizingMaskIntoConstraints = false
        productPriceLabel.topAnchor.constraint(equalTo: productBrandLabel.bottomAnchor).isActive = true
        productPriceLabel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        productPriceLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}






















