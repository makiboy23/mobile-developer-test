//
//  EmptyListCell.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 20/06/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import UIKit

class EmptyListCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    let activityIndicator: UIActivityIndicatorView = {
        let ai = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        ai.startAnimating()
        return ai
    }()
    
    let emptyMessageLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Empty Product List."
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 20)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    // MARK:- SETUPVIEWS
    
    fileprivate func setupViews() {
        addSubview(emptyMessageLabel)
        emptyMessageLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -100).isActive = true
        emptyMessageLabel.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        emptyMessageLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        addSubview(activityIndicator)
        activityIndicator.center = center
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
