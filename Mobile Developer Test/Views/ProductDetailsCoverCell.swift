//
//  ProductDetailsCoverCell.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 21/06/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import UIKit

class ProductDetailsCoverCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    var product: Product? {
        didSet {
            guard let imageUrl = product?.productImageUrl else { return }
            productImageView.loadImage(urlString: imageUrl)
        }
    }
    
    let productImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = .darkGray
        iv.tintColor = .lightGray
        iv.clipsToBounds = true
        iv.image = UIImage(named: "car-placeholder")?.withRenderingMode(.alwaysTemplate)
        return iv
    }()
    
    // MARK:- SETUPVIEWS
    
    fileprivate func setupViews() {
        addSubview(productImageView)
        productImageView.translatesAutoresizingMaskIntoConstraints = false
        productImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        productImageView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        productImageView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        productImageView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
