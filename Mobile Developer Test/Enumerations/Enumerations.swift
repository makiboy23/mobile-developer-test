//
//  Enumerations.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 20/06/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import Foundation

enum SortTypes: String {
    case sortAscDate = "oldest"
    case sortDescDate = "newest"
    case sortAscPrice = "price-low"
    case sortDescPrice = "price-high"
    case sortAscMiles = "mileage-low"
    case sortDescMiles = "mileage-high"
}

