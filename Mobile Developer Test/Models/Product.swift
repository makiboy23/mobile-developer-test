//
//  Product.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 20/06/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import Foundation

class Product: NSObject, Decodable, NSCoding {
    func encode(with aCoder: NSCoder) {
        aCoder.encode(productName ?? "", forKey: "productNameKey")
        aCoder.encode(productBrand ?? "", forKey: "productBrandKey")
        aCoder.encode(productPrice ?? "", forKey: "productPriceKey")
        aCoder.encode(productImageUrl ?? "", forKey: "productImageUrlKey")
        
        aCoder.encode(productCondition ?? "", forKey: "productConditionKey")
        aCoder.encode(personContactName ?? "", forKey: "personContactNameKey")
        aCoder.encode(personContactEmail ?? "", forKey: "personContactEmailKey")
        aCoder.encode(personContactMobile ?? "", forKey: "personContactMobileKey")
        aCoder.encode(personContactHomePhone ?? "", forKey: "personContactHomePhoneKey")
        // aCoder.encode(productImages ?? [ProductImage](), forKey: "productImagesKey")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.productName = aDecoder.decodeObject(forKey: "productNameKey") as? String
        self.productBrand = aDecoder.decodeObject(forKey: "productBrandKey") as? String
        self.productPrice = aDecoder.decodeObject(forKey: "productPriceKey") as? String
        self.productImageUrl = aDecoder.decodeObject(forKey: "productImageUrlKey") as? String
        
        self.productCondition = aDecoder.decodeObject(forKey: "productConditionKey") as? String
        self.personContactName = aDecoder.decodeObject(forKey: "personContactNameKey") as? String
        self.personContactEmail = aDecoder.decodeObject(forKey: "personContactEmailKey") as? String
        self.personContactMobile = aDecoder.decodeObject(forKey: "personContactMobileKey") as? String
        self.personContactHomePhone = aDecoder.decodeObject(forKey: "personContactHomePhoneKey") as? String
        // self.productImages = aDecoder.decodeObject(forKey: "productImagesKey") as? [ProductImage]
    }
    
    override init() {}
    
    var productName: String?
    var productBrand: String?
    var productPrice: String?
    var productImageUrl: String?
    
    var productCondition: String?
    var personContactName: String?
    var personContactEmail: String?
    var personContactMobile: String?
    var personContactHomePhone: String?
    
    var productImages: [ProductImage]?
}

class ProductImage: NSObject, Decodable {
    var imageUrl: String?
}
