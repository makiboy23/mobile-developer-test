//
//  APIServices.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 20/06/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import Foundation
import Alamofire

class APIServices {
    let baseUrl = "https://www.carmudi.co.id/api/cars"

    // singleton
    static let shared = APIServices()
    
    func fetchProducts(products: [Product] = [Product](), page: Int = 1, maxItem: Int = 10, sortType: SortTypes?, completionHandler: @escaping ([Product]) -> ()) {
        var url = "\(baseUrl)/page:\(page)/maxitems:\(maxItem)/"

        if sortType != nil {
            let sortTypeString = sortType?.rawValue ?? ""
            url = "\(baseUrl)/page:\(page)/maxitems:\(maxItem)/sort:\(sortTypeString)/"
        }
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default)
            .responseJSON { responseJSON in
                if let err = responseJSON.error {
                    print("Failed to contact server", err)
                    return
                }

                let result = responseJSON.result
                let products = result.toProducts(products: products)
                completionHandler(products)
        }
    }
}














