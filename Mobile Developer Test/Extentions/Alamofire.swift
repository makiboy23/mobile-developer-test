//
//  Alamofire.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 20/06/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import Alamofire

extension Result {
    func toProducts(products: [Product] = [Product]()) -> [Product] {
        let result = self
        switch result {
        case .success(let json) :
            guard let dictionary = json as? [String: Any] else { return [] }
            guard let metadata = dictionary["metadata"] as? [String: Any] else { return [] }
            guard let results = metadata["results"] as? [[String: Any]] else { return [] }

            var newProducts = products
            
            for array in results {
                let product = Product()
                
                guard let data = array["data"] as? [String: Any] else { continue }
                
                product.productName = data["original_name"] as? String ?? ""
                product.productBrand = data["brand"] as? String ?? ""
                product.productPrice = data["price"] as? String ?? "0"
                product.productCondition = data["condition"] as? String ?? ""
                product.personContactName = data["item_contact_name"] as? String ?? ""
                product.personContactEmail = data["item_contact_email"] as? String ?? ""
                product.personContactMobile = data["item_contact_mobile"] as? String ?? ""
                product.personContactHomePhone = data["item_contact_homephone"] as? String ?? ""
                
                if let imagesArray = array["images"] as? [[String: Any]] {
                    let images = fetchImages(imagesArray: imagesArray)
                    if images.count != 0 {
                        // get 1st image make it cover thumbnail photo
                        product.productImages = images
                        product.productImageUrl = images[0].imageUrl
                    }
                }
                
                UserDefaults.standard.setProduct(prodcuct: product)
                newProducts.append(product)
            }
            
            return newProducts
        case .failure(let error) :
            print("err:", error)
            return []
        }
    }
    
    fileprivate func fetchImages(imagesArray: [[String: Any]]) -> [ProductImage] {
        var productImages = [ProductImage]()
        for imageArray in imagesArray {
            let productImage = ProductImage()
            productImage.imageUrl = imageArray["url"] as? String ?? ""
            productImages.append(productImage)
        }
        return productImages
    }
    
}
















