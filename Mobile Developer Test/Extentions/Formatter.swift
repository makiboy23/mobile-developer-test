//
//  Formatter.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 20/06/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import Foundation

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = ","
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension String {
    var formattedWithSeparator: String {
        let value = self
        let number: NSNumber = NSNumber(value: Float(value) ?? 0)
        return Formatter.withSeparator.string(from: number) ?? ""
    }
}
