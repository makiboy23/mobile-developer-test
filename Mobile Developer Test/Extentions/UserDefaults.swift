//
//  UserDefaults.swift
//  Mobile Developer Test
//
//  Created by Marknel Pineda on 20/06/2018.
//  Copyright © 2018 mvpineda. All rights reserved.
//

import UIKit

extension UserDefaults {
    static let productListKey = "productListKey"
    
    // MARK: SET PRODUCT to offline
    func setProduct(prodcuct: Product) {
        var savedProducts = UserDefaults.standard.savedProducts()
        savedProducts.append(prodcuct)
        
        let data = NSKeyedArchiver.archivedData(withRootObject: savedProducts)
        UserDefaults.standard.set(data, forKey: UserDefaults.productListKey)
    }
    
    // get saved products
    func savedProducts() -> [Product] {
        guard let savedProductsData = UserDefaults.standard.data(forKey: UserDefaults.productListKey) else { return [] }
        
        guard let savedProducts = NSKeyedUnarchiver.unarchiveObject(with: savedProductsData) as? [Product] else { return [] }
        
        return savedProducts
    }
}













